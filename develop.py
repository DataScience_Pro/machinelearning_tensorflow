import pandas
from sklearn import model_selection
from sklearn.linear_model import LogisticRegression
import pickle
from sklearn.externals import joblib


url = "https://raw.githubusercontent.com/jbrownlee/Datasets/master/pima-indians-diabetes.data.csv"
names = ['preg', 'plas', 'pres', 'skin', 'test', 'mass', 'pedi', 'age', 'class']
dataframe = pandas.read_csv(url, names=names)
array = dataframe.values

x = array[:, 0:8]   # first: row number, second: column number
y = array[:, 8]

test_size = 0.33
seed = 7
X_train, X_test, Y_train, Y_test = model_selection.train_test_split(x, y, test_size=test_size, random_state=seed)

model = LogisticRegression()
model.fit(X_train, Y_train)

# save model to disk
filename = 'final_model.sav'
pickle.dump(model, open(filename, 'wb'))
filename_joblib = 'final_joblib_model.sav'
joblib.dump(model, filename_joblib)
print('finish')

# load the model from disk
loaded_model = pickle.load(open(filename, 'rb'))
result = loaded_model.score(X_test, Y_test)
print(result)

loaded_model_joblib = joblib.load(filename_joblib)
result_joblib = loaded_model_joblib.score(X_test, Y_test)
print(result_joblib)
