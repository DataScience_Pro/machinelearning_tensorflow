# README #

This is Machine Learning Project with Tensorflow.

## Issues
### AVX2 Problem
Run Code:

    import tensorflow as tf
    hello = tf.constant('Hello, TensorFlow!')
    sess = tf.Session()
    print(sess.run(hello))

Warning Msg:

    Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2

Resolved Code:

    import os
    import tensorflow as tf
    
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
    hello = tf.constant('Hello, TensorFlow!')
    sess = tf.Session()
    print(sess.run(hello))

Then, warning is resolved.
